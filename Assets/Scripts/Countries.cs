﻿using UnityEngine;

public class Countries : MonoBehaviour
{
    [SerializeField] private Country[] _countries = null;
    
#if UNITY_EDITOR
    [ContextMenu(nameof(FillCountries))]
    private void FillCountries()
    {
        _countries = GetComponentsInChildren<Country>();
    }
#endif
}
