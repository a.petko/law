﻿using UnityEngine;

public sealed class Country : MonoBehaviour
{
    [SerializeField] private MeshRenderer _meshRenderer = null;
    [SerializeField] private string _id;
    
    private static readonly int ColorId = Shader.PropertyToID("_EmissionColor");
    private MaterialPropertyBlock _materialPropertyBlock;

    private bool _isHighlight;

    public string Id => _id;
    
    private void Awake()
    {
        _materialPropertyBlock = new MaterialPropertyBlock();
    }

    public void Highlight()
    {
        if (_isHighlight)
            return;
        
        _isHighlight = true;
        SetColor(new Color(0.09859414f, 0f, 0.3867925f));
    }

    public void Hide()
    {
        if (!_isHighlight)
            return;
        
        _isHighlight = false;
        SetColor(Color.black);
    }
    
    private void SetColor(Color color)
    {
        _materialPropertyBlock.SetColor(ColorId, color);
        _meshRenderer.SetPropertyBlock(_materialPropertyBlock);
    }
}