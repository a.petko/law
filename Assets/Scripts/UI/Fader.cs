﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public sealed class Fader : MonoBehaviour
    {
        private const float Duration = 1f;

        private bool _isCreated;
        private Image[] _images;
        private TMP_Text[] _texts;
        
        private void Awake()
        {
            TryCreate();
        }

        public void Fade(float endValue)
        {
            TryCreate();
            foreach (var item in _images)
            {
                var color = item.color;
                color.a = endValue;
                item.color = color;
            }

            foreach (var item in _texts)
            {
                var color = item.color;
                color.a = endValue;
                item.color = color;
            }
        }
        
        public Tween DOFade(float endValue)
        {
            TryCreate();
            var sequence = DOTween.Sequence();
            foreach (var item in _images)
                sequence.Join(item.DOFade(endValue, Duration));
            foreach (var item in _texts)
                sequence.Join(item.DOFade(endValue, Duration));
            return sequence;
        }

        private void TryCreate()
        {
            if (_isCreated)
                return;
            _isCreated = true;
            _images = GetComponentsInChildren<Image>(true);
            _texts = GetComponentsInChildren<TMP_Text>(true);
        }
    }
}
