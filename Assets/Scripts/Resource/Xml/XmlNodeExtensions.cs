﻿
using System.Xml;
using UnityEngine;

namespace Resource.Xml
{
    internal static class XmlNodeExtensions
    {
        internal static XmlNode GetNodeWithLogError(this XmlNode parent, string nodeName)
        {
            var node = parent[nodeName];
            if (node == null)
                Debug.LogError(Errors.NoNodeFound(nodeName));

            return node;
        }
    }
}