﻿
namespace Resource.Xml
{
    public class AttackData
    {
        public float? StressAggressor;
        public float? StressTarget;
        public int? Reward;
        public int? Penalty;
    }
}