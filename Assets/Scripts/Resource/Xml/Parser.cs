﻿using System.Collections.Generic;
using System.Globalization;
using System.Xml;

namespace Resource.Xml
{
    internal static class Parser
    {
        internal static Dictionary<(string, string), AttackData> ParseAttack(string text)
        {
            var attacks = Attacks(text);
            var dic = new Dictionary<(string, string), AttackData>();
            foreach (var attack in attacks)
            {
                var key = (attack.Name, attack.Other);
                if (!dic.ContainsKey(key))
                    dic[key] = new AttackData();

                dic[key].StressAggressor = attack.StressAggressor.Convert(dic[key].StressAggressor);
                dic[key].StressTarget = attack.StressTarget.Convert(dic[key].StressTarget);
                dic[key].Reward = attack.Reward.Convert(dic[key].Reward);
                dic[key].Penalty = attack.Penalty.Convert(dic[key].Penalty);
            }

            return dic;
        }

        internal static Info ParseInfo(string text)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(text);

            var root = xmlDoc.GetNodeWithLogError("root");

            var nameElement = root?["name"];
            if (nameElement == null)
                return null;

            var info = new Info(nameElement.InnerText);

            return info;
        }

        private static RawAttackData[] Attacks(string text)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(text);

            var root = xmlDoc.GetNodeWithLogError("root");

            var attacks = root?.SelectNodes("//attack");
            if (attacks == null)
                return null;

            var attackDatas = new RawAttackData[attacks.Count];
            for (var i = 0; i < attacks.Count; ++i)
                attackDatas[i] = attacks[i].Attributes.Attack();

            return attackDatas;
        }

        private static RawAttackData Attack(this XmlAttributeCollection attributes)
        {
            return new RawAttackData
            {
                Name = attributes.Get("name"),
                Other = attributes.Get("other"),
                StressAggressor = attributes.Get("stress-aggressor"),
                StressTarget = attributes.Get("stress-target"),
                Reward = attributes.Get("reward"),
                Penalty = attributes.Get("penalty"),
            };
        }

        private static string Get(this XmlAttributeCollection attributes, string attributeName)
            => attributes[attributeName]?.Value;

        private static float? Convert(this string value, float? def) =>
            value == null || !float.TryParse(
                value, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out var result)
                ? def
                : result;

        private static int? Convert(this string value, int? def) =>
            value == null || !int.TryParse(value, out var result) ? def : result;

        private class RawAttackData
        {
            public string Name;
            public string Other;
            public string StressAggressor;
            public string StressTarget;
            public string Reward;
            public string Penalty;
        }
    }
}