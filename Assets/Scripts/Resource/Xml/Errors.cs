﻿
namespace Resource.Xml
{
    internal static class Errors
    {
        internal static string NoNodeFound(string node)
            => $"no node {node} found";

        internal static string NoAttribute(string attribute)
            => $"the node has no {attribute} attribute";

        internal static string NoConversionPossible(string node, string attribute, string value, string type)
            => $"{node}.{attribute} no conversion possible from {value} to {type}";
    }
}