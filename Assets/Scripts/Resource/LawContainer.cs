﻿using System.Collections.Generic;
using Resource.Xml;

namespace Resource
{
    public class InfoContainer
    {
        private readonly Dictionary<string, Info> _data;

        public InfoContainer(Dictionary<string, Info> data)
        {
            _data = data;
        }
        
        public Info Get(string code) => _data[code];
    }
    
    public class LawContainer
    {
        private readonly Dictionary<string, Dictionary<(string, string), AttackData>> _data;

        public LawContainer(Dictionary<string, Dictionary<(string, string), AttackData>> data)
        {
            _data = data;
        }
        
        public Data Get(string aggressor, string target, string attack)
        {
            var answer = new DataBuilder();
            var targetData = _data[target];
            var aggressorData = _data[aggressor];
            var defaultData = _data["default"];
            
            var key0 = (attack, aggressor);
            var key1 = ((string) null, aggressor);
            var key2 = (attack, (string)null);
            if (targetData.ContainsKey(key0) && targetData[key0].StressTarget.HasValue)
                answer.StressTarget = targetData[key0].StressTarget.Value;
            else if (targetData.ContainsKey(key1) && targetData[key1].StressTarget.HasValue)
                answer.StressTarget = targetData[key1].StressTarget.Value;
            else if (targetData.ContainsKey(key2) && targetData[key2].StressTarget.HasValue)
                answer.StressTarget = targetData[key2].StressTarget.Value;
            else if (defaultData.ContainsKey(key2) && defaultData[key2].StressTarget.HasValue)
                answer.StressTarget = defaultData[key2].StressTarget.Value;

            if (targetData.ContainsKey(key0) && targetData[key0].Reward.HasValue)
                answer.Reward = targetData[key0].Reward.Value;
            else if (targetData.ContainsKey(key1) && targetData[key1].Reward.HasValue)
                answer.Reward = targetData[key1].Reward.Value;
            else if (targetData.ContainsKey(key2) && targetData[key2].Reward.HasValue)
                answer.Reward = targetData[key2].Reward.Value;
            else if (defaultData.ContainsKey(key2) && defaultData[key2].Reward.HasValue)
                answer.Reward = defaultData[key2].Reward.Value;
            
            if (targetData.ContainsKey(key0) && targetData[key0].Penalty.HasValue)
                answer.PenaltyTarget = targetData[key0].Penalty.Value;
            else if (targetData.ContainsKey(key1) && targetData[key1].Penalty.HasValue)
                answer.PenaltyTarget = targetData[key1].Penalty.Value;
            else if (targetData.ContainsKey(key2) && targetData[key2].Penalty.HasValue)
                answer.PenaltyTarget = targetData[key2].Penalty.Value;
            else if (defaultData.ContainsKey(key2) && defaultData[key2].Penalty.HasValue)
                answer.PenaltyTarget = defaultData[key2].Penalty.Value;
            
            key0 = (attack, target);
            if (aggressorData.ContainsKey(key0) && aggressorData[key0].StressAggressor.HasValue)
                answer.StressAggressor = aggressorData[key0].StressAggressor.Value;
            else if (aggressorData.ContainsKey(key1) && aggressorData[key1].StressAggressor.HasValue)
                answer.StressAggressor = aggressorData[key1].StressAggressor.Value;
            else if (aggressorData.ContainsKey(key2) && aggressorData[key2].StressAggressor.HasValue)
                answer.StressAggressor = aggressorData[key2].StressAggressor.Value;
            else if (defaultData.ContainsKey(key2) && defaultData[key2].StressAggressor.HasValue)
                answer.StressAggressor = defaultData[key2].StressAggressor.Value;
            
            if (aggressorData.ContainsKey(key0) && aggressorData[key0].Penalty.HasValue)
                answer.PenaltyAggressor = aggressorData[key0].Penalty.Value;
            else if (aggressorData.ContainsKey(key1) && aggressorData[key1].Penalty.HasValue)
                answer.PenaltyAggressor = aggressorData[key1].Penalty.Value;
            else if (aggressorData.ContainsKey(key2) && aggressorData[key2].Penalty.HasValue)
                answer.PenaltyAggressor = aggressorData[key2].Penalty.Value;
            else if (defaultData.ContainsKey(key2) && defaultData[key2].Penalty.HasValue)
                answer.PenaltyAggressor = defaultData[key2].Penalty.Value;
           
            return answer;
        }

        public HashSet<string> GetAllAttacks()
        {
            var result = new HashSet<string>();
            foreach (var countyData in _data.Values)
            foreach (var (attackName, _) in countyData.Keys)
                result.Add(attackName);
            return result;
        }
        
        public readonly struct Data
        {
            public readonly int Reward;
            public readonly int PenaltyAggressor;
            public readonly int PenaltyTarget;
            public readonly float StressAggressor;
            public readonly float StressTarget;

            public Data(int reward, int penaltyAggressor, int penaltyTarget, float stressAggressor, float stressTarget)
            {
                Reward = reward;
                PenaltyAggressor = penaltyAggressor;
                PenaltyTarget = penaltyTarget;
                StressAggressor = stressAggressor;
                StressTarget = stressTarget;
            }
        }

        private struct DataBuilder
        {
            public int Reward;
            public int PenaltyAggressor;
            public int PenaltyTarget;
            public float StressAggressor;
            public float StressTarget;
            
            public static implicit operator Data(DataBuilder self) 
                => new Data(self.Reward, self.PenaltyAggressor, self.PenaltyTarget, self.StressAggressor, self.StressTarget);
        }
    }
}