﻿namespace Resource
{
    public class Info
    {
        public readonly string Name;

        public Info(string name)
        {
            Name = name;
        }
    }
}