﻿using System.Collections.Generic;
using System.IO;
using Resource.Xml;

namespace Resource
{
    public class WindowLoader
    {
        public Dictionary<string, Dictionary<(string, string), AttackData>> LoadAllAttacks()
        {
            var result = new Dictionary<string, Dictionary<(string, string), AttackData>>();
            foreach (var (key, text) in ReadAll("attack.xml"))
                result[key] = Parser.ParseAttack(text);

            return result;
        }

        public Dictionary<string, Info> LoadAllInfos()
        {
            var result = new Dictionary<string, Info>();
            foreach (var (key, text) in ReadAll("info.xml")) 
                result[key] = Parser.ParseInfo(text);

            return result;
        }

        private IEnumerable<(string, string)> ReadAll(string filename)
        {
            foreach (var dir in GetDirectories())
            {
                var path = Path.Combine(dir, filename);
                if (!File.Exists(path))
                    continue;
                
                var key = Path.GetFileName(dir);
                var text = File.ReadAllText(path);
                yield return (key, text);
            }
        }
        
        private string[] GetDirectories()
        {
            var resourcePath = Path.Combine(Directory.GetCurrentDirectory(), "Resources");
            var countriesDirPath = Path.Combine(resourcePath, "Countries");
            return Directory.GetDirectories(countriesDirPath);
        }
    }
}