﻿using System;
using UI;
using UnityEngine;

namespace Pages
{
    public class Page : MonoBehaviour
    {
        [SerializeField] private Fader _fader = null;
        
        private bool _isCreated;
        private IPageStack _pageStack;

        public PageState PageState { get; private set; } = PageState.Closed;

        public event Action OpenStarted;
        public event Action Opened;
        public event Action CloseStarted;
        public event Action Closed;
        
        private void Awake()
        {
            TryCreate();
        }

        public void SetStack(IPageStack pageStack)
        {
            _pageStack = pageStack;
            TryCreate();
        }

        public void OpenInStack()
        {
            _pageStack?.OpenInStack(this);
        }
        
        public void Open()
        {
            OnOpen();
        }
        
        public void Close()
        {
            OnClose();
        }

        public void Back()
        {
            _pageStack?.Back();
        }

        protected virtual void OnCreate()
        {
            // nothing
        }
        
        protected virtual void OnOpen()
        {
            PageState = PageState.Opening;
            OpenStarted?.Invoke();
            gameObject.SetActive(true);
            _fader.DOFade(1f).onComplete += () =>
            {
                PageState = PageState.Opened;
                Opened?.Invoke();
            };
        }
        
        protected virtual void OnClose()
        {
            PageState = PageState.Closing;
            CloseStarted?.Invoke();
            _fader.DOFade(0f).onComplete += () =>
            {
                gameObject.SetActive(false);
                PageState = PageState.Closed;
                Closed?.Invoke();
            };
        }

        private void TryCreate()
        {
            if (_isCreated)
                return;
            _isCreated = true;

            _fader.Fade(0f);
            OnCreate();
        }
    }
}