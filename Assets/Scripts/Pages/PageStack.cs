﻿using System.Collections.Generic;
using UnityEngine.UI;

namespace Pages
{
    public sealed class PageStack : IPageStack
    {
        private readonly GraphicRaycaster _raycaster;
        private readonly Stack<Page> _stack = new Stack<Page>();
        private Page _needOpen;
        private Page _waitClose;

        public PageStack(GraphicRaycaster raycaster)
        {
            _raycaster = raycaster;
        }
        
        public void OpenInStack(Page page)
        {
            if (_waitClose == null && _stack.Count > 0) 
                _waitClose = _stack.Peek();

            _needOpen = page;
        }

        public void OpenMain()
        {
            if (_stack.Count <= 1)
                return;
            
            if (_waitClose == null) 
                _waitClose = _stack.Pop();
            
            while (_stack.Count > 1)
                _stack.Pop();
            
            _needOpen = _stack.Pop();
        }
        
        public void Back()
        {
            if (_stack.Count <= 1)
                return;

            if (_waitClose == null) 
                _waitClose = _stack.Pop();
            
            _needOpen = _stack.Pop();
        }

        public void Update()
        {
            _raycaster.enabled = false;
            if (_waitClose != null)
            {
                if (_waitClose.PageState == PageState.Opening || 
                    _waitClose.PageState == PageState.Closing)
                    return;
                
                if (_waitClose.PageState == PageState.Opened)
                {
                    _waitClose.Close();
                }

                if (_waitClose.PageState == PageState.Closed) 
                    _waitClose = null;
            }

            if (_waitClose != null)
                return;

            if (_needOpen != null)
            {
                if (_needOpen.PageState == PageState.Opening || 
                    _needOpen.PageState == PageState.Closing)
                    return;
                
                if (_needOpen.PageState == PageState.Closed)
                {
                    _needOpen.Open();
                    _stack.Push(_needOpen);
                }

                if (_needOpen.PageState == PageState.Opened)
                    _needOpen = null;
                return;
            }
            
            _raycaster.enabled = true;
        }
    }
}