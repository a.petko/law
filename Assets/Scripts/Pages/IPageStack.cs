﻿namespace Pages
{
    public interface IPageStack
    {
        void OpenInStack(Page page);
        void OpenMain();
        void Back();
        void Update();
    }
}