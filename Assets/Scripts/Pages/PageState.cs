﻿namespace Pages
{
    public enum PageState
    {
        Opening,
        Opened,
        Closing,
        Closed,
    }
}