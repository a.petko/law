﻿using System;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pages
{
    public sealed class HackPage : Page
    {
        [SerializeField] private Button _cancelButton = null;
        [Space]
        [SerializeField] private Button _attackButton = null;
        [SerializeField] private TMP_Text _attackText = null;
        [Space]
        [SerializeField] private Button _fromButton = null;
        [SerializeField] private TMP_Text _fromText = null;
        [Space]
        [SerializeField] private Button _toButton = null;
        [SerializeField] private TMP_Text _toText = null;

        public event Action AttackClicked;
        public event Action FromClicked;
        public event Action ToClicked;
        
        protected override void OnCreate()
        {
            _cancelButton.onClick.AddListener(Back);
            _attackButton.onClick.AddListener(() => AttackClicked?.Invoke());
            _fromButton.onClick.AddListener(() => FromClicked?.Invoke());
            _toButton.onClick.AddListener(() => ToClicked?.Invoke());
            _attackText.text = Directory.GetCurrentDirectory();
        }

        public void SetAttack(string name)
        {
            
        }

        public void SetFrom()
        {
            
        }

        public void SetTo()
        {
            
        }
    }
}
