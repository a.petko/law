﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Pages
{
    public sealed class MainPage : Page
    {
        [SerializeField] private Button _button = null;

        public event Action StartHackClicked;

        protected override void OnCreate()
        {
            _button.onClick.AddListener(() => StartHackClicked?.Invoke());
        }
    }
}
