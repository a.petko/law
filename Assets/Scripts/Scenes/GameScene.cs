﻿using System.IO;
using DG.Tweening;
using FsmSystem;
using FsmSystem.Main;
using Pages;
using Resource;
using UnityEngine;
using UnityEngine.UI;

namespace Scenes
{
    public sealed class GameScene : MonoBehaviour
    {
        [SerializeField] private Camera _camera = null;
        [SerializeField] private Transform _planetCameraContainer = null;
        [SerializeField] private Transform _spaceCameraContainer = null;
        [SerializeField] private SphereCollider _sphereCollider = null;
        [SerializeField] private GraphicRaycaster _graphicRaycaster = null;
        [Space]
        [SerializeField] private MainPage _mainPage = null;
        [SerializeField] private HackPage _hackPage = null;
        [SerializeField] private CountrySelectPage _countrySelectPage = null;

        private readonly Fsm<State, MouseEvent> _navigationFsm = new Fsm<State, MouseEvent>();
        private LawContainer _lawContainer;
        private InfoContainer _infoContainer;
        
        private PageStack _pageStack;

        private void Awake()
        {
            InitStates();
            InitPages();
            InitResources();
            Debug.Log("RE");
        }

        private void Update()
        {
            if (Input.GetMouseButton(1))
                _planetCameraContainer.rotation = Quaternion.RotateTowards(
                    _planetCameraContainer.rotation,
                    Quaternion.identity,
                    100 * Time.deltaTime);
            
            _pageStack.Update();
            _navigationFsm.Current.Update();
        }

        private void InitStates()
        {
            var scrollBehavior = new ScrollBehavior(_camera.transform);
            
            var wait = new WaitState(_camera, scrollBehavior);
            var drag = new DragState(
                _camera, _planetCameraContainer, scrollBehavior, _sphereCollider.radius);
            var none = new NoneState();
            
            wait.DragBegan += () => _navigationFsm.Fire(MouseEvent.Down);
            drag.DragEnded += () => _navigationFsm.Fire(MouseEvent.Up);
            _mainPage.CloseStarted += () => _navigationFsm.Fire(MouseEvent.Disable);
            
            _navigationFsm
                .Add(wait, drag, MouseEvent.Down)
                .Add(drag, wait, MouseEvent.Up)
                .Add(none, wait, MouseEvent.Enable)
                .AddGlobal(none, MouseEvent.Disable)
                .Start(wait);
        }

        private void InitPages()
        {
            _pageStack = new PageStack(_graphicRaycaster);
            _mainPage.SetStack(_pageStack);
            _hackPage.SetStack(_pageStack);

            _mainPage.OpenStarted += () =>
            {
                DOCameraMove(_planetCameraContainer, Vector3.zero)
                    .OnComplete(() => _navigationFsm.Fire(MouseEvent.Enable));
            };

            _hackPage.OpenStarted += () =>
            {
                _spaceCameraContainer.rotation = _planetCameraContainer.rotation;
                DOCameraMove(_spaceCameraContainer, new Vector3(-26f, 45f, 0f));
            };
            _mainPage.StartHackClicked += () => _hackPage.OpenInStack();
            
            _mainPage.OpenInStack();
        }

        private void InitResources()
        {
            var loader = new WindowLoader();
            _lawContainer = new LawContainer(loader.LoadAllAttacks());
            _infoContainer = new InfoContainer(loader.LoadAllInfos());
        }
        
        private Tween DOCameraMove(Transform container, Vector3 rot, float z = -50f, float 
        duration = 1f)
        {
            var tran = _camera.transform;
            tran.SetParent(container);
            return DOTween.Sequence()
                .Join(tran.DOLocalMove(new Vector3(0f, 0f, z), duration))
                .Join(tran.DOLocalRotate(rot, duration))
                .SetTarget(tran);
        }
    }
}