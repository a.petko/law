﻿namespace FsmSystem
{
    public interface IState
    {
        void Enter();
        void Exit();
    }
}