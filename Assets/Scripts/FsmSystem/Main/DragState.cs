﻿using System;
using UnityEngine;

namespace FsmSystem.Main
{
    public sealed class DragState : State
    {
        private readonly Camera _camera;
        private readonly Transform _target;
        private readonly ScrollBehavior _scrollBehavior;
        private readonly float _radius;

        private Vector3 _startPosition;
        private Quaternion _startRotation;

        public event Action DragEnded;
        
        public DragState(Camera camera, Transform target, ScrollBehavior scrollBehavior, float radius)
        {
            _camera = camera;
            _target = target;
            _scrollBehavior = scrollBehavior;
            _radius = radius;
        }

        public override void Enter()
        {
            _startPosition = Input.mousePosition;
            _startRotation = _target.rotation;
        }

        public override void Update()
        {
            if (Input.GetMouseButtonUp(0))
            {
                DragEnded?.Invoke();
                return;
            }

            _scrollBehavior.Update();
            
            var distance = Vector3.Distance(_camera.transform.position, _target.position);

            var world = GetScreenInWorldSize(distance);
            var deltaPercent = GetDeltaPercent();

            var worldDelta = world * deltaPercent;
            var planetPercent = worldDelta / _radius;
            var sector = planetPercent *  Mathf.Rad2Deg;

            var rotation = Quaternion.Euler(-sector.y, sector.x, 0f);
            _target.rotation = _startRotation * rotation;
        }

        private Vector2 GetDeltaPercent()
        {
            var delta = Input.mousePosition - _startPosition;
            var screen = new Vector2(Screen.width, Screen.height);
            var percent = delta / screen;
            return percent;
        }

        private Vector2 GetScreenInWorldSize(float distance)
        {
            var world = _camera.ViewportToWorldPoint(new Vector3(1f, 1f, distance));
            world = _camera.transform.worldToLocalMatrix.MultiplyPoint(world);
            return world;
        }
    }
}