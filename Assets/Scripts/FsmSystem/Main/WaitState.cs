﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace FsmSystem.Main
{
    public sealed class WaitState : State
    {
        private readonly Camera _camera;
        private readonly ScrollBehavior _scrollBehavior;
        private readonly float _dragDuration;

        private Country _country;
        private float _duration;
        private bool _isDown;

        public event Action DragBegan;
        
        public WaitState(Camera camera, ScrollBehavior scrollBehavior, float dragDuration = 0.2f)
        {
            _camera = camera;
            _scrollBehavior = scrollBehavior;
            _dragDuration = dragDuration;
        }

        public override void Update()
        {
            if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                _duration = 0f;
                _isDown = true;
            }

            if (Input.GetMouseButtonUp(0))
            {
                _isDown = false;
                return;
            }

            if (_isDown)
            {
                _duration += Time.deltaTime;
                if (_duration >= _dragDuration)
                {
                    _isDown = false;
                    DragBegan?.Invoke();
                    return;
                }
            }

            _scrollBehavior.Update();

            CountriesProcess();
        }

        public override void Exit()
        {
            LoseCountry();
        }

        private void CountriesProcess()
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                LoseCountry();
                return;
            }
            
            var input = Input.mousePosition;
            input.z = 1f;
            var ray = _camera.ScreenPointToRay(input);
            
            if (!Physics.Raycast(ray, out var hit))
                return;
        
            if (!hit.collider.gameObject.TryGetComponent(out Country country))
            {
                LoseCountry();
                return;
            }
        
            _country = country;
            country.Highlight();
        }
        
        private void LoseCountry()
        {
            if (_country == null) 
                return;
                
            _country.Hide();
            _country = null;
        }
    }
}