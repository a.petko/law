﻿namespace FsmSystem.Main
{
    public enum MouseEvent
    {
        Down,
        Up,
        Enable,
        Disable,
    }
}