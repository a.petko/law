﻿using System;
using UnityEngine;

namespace FsmSystem.Main
{
    public sealed class ScrollBehavior : IBehavior
    {
        private readonly Transform _target;

        public ScrollBehavior(Transform target)
        {
            _target = target;
        }

        public void Update()
        {
            var mouseScrollDelta = Input.mouseScrollDelta;
            if (Math.Abs(mouseScrollDelta.y) < 0.1f) 
                return;
            
            var position = _target.localPosition;
            position.z += mouseScrollDelta.y;
            _target.localPosition = position;
        }
    }
}