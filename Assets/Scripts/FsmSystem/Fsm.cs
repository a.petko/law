﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine.Assertions;

namespace FsmSystem
{
    public class Fsm<TS, TE> where TS : class, IState
    {
        private readonly Dictionary<TE, TS> _globalMap = new Dictionary<TE, TS>();
        private readonly Dictionary<TS, Dictionary<TE, TS>> _translationMap
            = new Dictionary<TS, Dictionary<TE, TS>>();

        public TS Current { get; private set; }

        public Fsm<TS, TE> Add([NotNull] TS from, [NotNull] TS to, TE e)
        {
            Assert.IsNotNull(from);
            Assert.IsNotNull(to);
            
            if (!_translationMap.ContainsKey(from))
                _translationMap.Add(from, new Dictionary<TE, TS>());

            var eventMap = _translationMap[from];
            if (eventMap.ContainsKey(e))
                throw new ArgumentException(
                    "the collection already contains these arguments");

            eventMap[e] = to;
            return this;
        }

        public Fsm<TS, TE> AddGlobal([NotNull] TS to, TE e)
        {
            Assert.IsNotNull(to);
            
            if (_globalMap.ContainsKey(e))
                throw new ArgumentException(
                    "the collection already contains these arguments");

            _globalMap[e] = to;
            return this;
        }
        
        public void Start([NotNull] TS state)
        {
            Assert.IsNotNull(state);

            Current = state;
            Current.Enter();
        }

        public void Fire(TE e)
        {
            if (!_translationMap.ContainsKey(Current) || !_translationMap[Current].ContainsKey(e))
            {
                if (_globalMap.ContainsKey(e) && !Current.Equals(_globalMap[e]))
                {
                    Current.Exit();
                    Current = _globalMap[e];
                    Current.Enter();
                }
                return;
            }

            Current.Exit();
            Current = _translationMap[Current][e];
            Current.Enter();
        }
    }
}