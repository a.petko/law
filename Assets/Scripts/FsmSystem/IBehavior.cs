﻿namespace FsmSystem
{
    public interface IBehavior
    {
        void Update();
    }
}